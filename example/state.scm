(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit "00ff6f7c399670a76efffb91276dea2633cc130c"))
      (channel
       (name 'guix-cran)
       (url "https://github.com/guix-science/guix-cran")
       (branch "master")
       (commit "ab70c9b745a0d60a40ab1ce08024e1ebca8f61b9"))
      (channel
       (name 'my-team)
       (url "https://my-forge.my-institute.xyz/my-custom-channel")
       (branch "main")
       (commit "ab70c9b745a0d60a40ab1ce08024e1ebca8f61b9")))
