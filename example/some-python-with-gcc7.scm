(use-modules (guix transformations))

(define transform
  (options->transformation
   '((with-c-toolchain . "python=gcc-toolchain@7"))))

(packages->manifest
 (map (compose transform specification->package)
      (list
       "python"
       "python-matplotlib"
       "python-numpy"
       "python-scipy")))
