#!/usr/bin/env bash

for ext in vrb aux log nav out snm toc;
do
    find . -name "*.${ext}" -type f -print -exec rm {} \;
done
